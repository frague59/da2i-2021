# Application de Carnet d'adresse

**Cette application est destinée à l'apprentissage basique de Django, pour les étudiants de Licence Pro DA2I le L'unicersité de Lille - IUT-A**

## Objectif

Faire découvrir le Framework Django aux étudiants.
L'installation de base de l'application a été effectuée, et quelques réglages ont été ajoutés :

## Pour commencer...

```sh
$ git clone https://gitlab.com/frague59/da2i-2021.git
$ cd da2i-2021/
$ python3 -m venv ./venv  # Installation de venv
$ . venv/bin/activate
(venv)$ pip install -r requirements/common.txt
(venv)$ pip install -r requirements/develop.txt
(venv)$ cd src/
(venv)$ ./manage.py
```

## Attendus

Votre chef de projet vous demande de faire une application de carnet d'adresse des projets.
Les projets sont nommées "Project", et un utilisateur peut travailler sur plusieurs projets.

### Modèle de données

#### Administrable

Cette classe est une classe de modèle **abstraite**, qui permet d'enregistrer l'utilisateur qui a saisi les informations dans la table concernée :

+ Utilisateur créateur
+ Date de création
+ Utilisateur updater
+ Date de mise à jour

**Cette classe est déjà implémentés dans peoples/models/helpers.py**

| Name | Type | Nullable ? | Desc. |
|---|---|---|---|
| created_by | User | No | Automatic on create |
| created_at | datetime | No | Automatic on create |
| updated_by | User | Yes | Automatic on update |
| updated_at | datetime | Yes | Automatic on update |

#### Job (Administrable)

| Name | Type | Nullable ? | Desc. |
|---|---|---|---|
| label | Char (255) | No | |
| description | Text | Yes | Desc. |

**Jobs à ajouter à la base de données :**

+ Admin sys
+ Chef de projet
+ Développeur
+ Directeur technique
+ Resposnable sécurité

#### Civility (Administrable)

| Name | Type | Nullable ? | Desc. |
|---|---|---|---|
| label | Char (255) | No | |
| label_short | Char (255) | Yes | Short version, aka. M. for Mister |

**Civilités à ajouter à la base de données :**

+ Monsieur / M.
+ Madame / Mme
+ Docteur / Dr
+ Maître / Me
+ Professeur / Pr


#### Project (Administrable)

| Name | Type | Nullable ? | Desc. |
|---|---|---|---|
| label | Char (255) | No | Name of the workgroup |
| description | Text | Yes | Desc. |

⚠ Les projets ont des "members"

**Projets à ajouter à la base de données :**

+ Ariane | Lanceur de satellites européen
+ Soyouz | Véhicule spacial russe
+ Proton | Lanceur de satellites russe
+ Starship SN9 | Véhicule orbital réutilisable (SpaceX)

#### People

| Name | Type | Nullable ? | Desc. |
|---|---|---|---|
| last_name | Char (255) | No | last name |
| first_name | Char (255) | No | first name |
| civility | FK -> Civility | No | FK to civility table |
| job | FK -> Job | Yes | A people has 1 job |
| email | \<email address\> | No | email address |
| phone_number | Char(50) | Yes | Phone number |
| projects | M2M -> (WorkGroup) | Yes | A people can be member of many groups ! |

## Déroulement de la session

+ [x] Découverte de django
+ [x] Implémentation du modèle de données
+ [ ] Interface d'administration
  + [ ] Liste des projects avec leurs membres dans la liste
  + [ ] Liste des personnes avec leurs projets dans la liste
+ [ ] Implémentation de vues "basique" (CBV)
  + [ ] Liste des personnes
  + [ ] Détail d'une personne (avec ses projets)