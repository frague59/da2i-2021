import logging
from django.contrib import admin
from peoples import models

logger = logging.getLogger(__name__)

admin.site.site_header = "Administration de l'annuaire"
admin.site.site_title = "Annuaire de l'agence"


@admin.register(models.Civility)
class CivilityAdmin(admin.ModelAdmin):
    list_display = (
        "label",
        "label_short",
    )


@admin.register(models.Job)
class JobAdmin(admin.ModelAdmin):
    list_display = (
        "label",
        "description",
    )
