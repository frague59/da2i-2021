"""
Views for the :mod:`peoples` views
"""
import logging
from django.views.generic import ListView
from peoples import models

logger = logging.getLogger(__name__)


class PeopleListView(ListView):
    model = models.People
