import logging
from django.conf import settings
from django.db import models
from django_currentuser.middleware import get_current_authenticated_user

logger = logging.getLogger(__name__)


class Administrable(models.Model):
    """
    Provides the administrative fields to a model: created by / at and updated by / at
    """

    #: Creation date
    created_at = models.DateTimeField(
        verbose_name="created at",
        auto_now_add=True,
        editable=False,
    )

    #: Last update date
    updated_at = models.DateTimeField(
        verbose_name="updated at",
        auto_now=True,
        editable=False,
        null=True,
    )

    #: Initial creator
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name="creator",
        editable=False,
        related_name="%(app_label)s_%(class)s_creators",
        on_delete=models.PROTECT,
    )

    #: Last updater
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name="updater",
        editable=False,
        null=True,
        blank=True,
        related_name="%(app_label)s_%(class)s_updaters",
        on_delete=models.PROTECT,
    )

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        current_user = get_current_authenticated_user()
        if self.pk:
            # The object already exists in the database
            self.updated_by = current_user
            logger.debug(
                f"{self.__class__.__name__}::save() The (last) updater has been set..."
            )
        else:
            self.created_by = current_user
            logger.debug(
                f"{self.__class__.__name__}::save() The creator has been added..."
            )

        super().save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
        )

    class Meta:
        abstract = True
