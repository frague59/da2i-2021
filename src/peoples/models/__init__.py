"""
Models for the :mod:`peoples application`
"""
import logging
from django.db import models

from .helpers import Administrable

logger = logging.getLogger(__name__)


class Civility(Administrable):
    label = models.CharField(
        max_length=255,
        verbose_name="label",
    )

    label_short = models.CharField(
        max_length=255,
        verbose_name="Short version",
        help_text="The short version will be displayed on titles",
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = "Civility"
        verbose_name_plural = "Civilities"
        ordering = ("label",)


class Job(Administrable):
    label = models.CharField(
        max_length=255,
    )

    description = models.TextField(
        verbose_name="Desc.",
        null=True,
    )

    class Meta:
        verbose_name = "Job"
        verbose_name_plural = "Jobs"
        ordering = ("label",)


class Project(Administrable):
    label = models.CharField(
        max_length=255,
        verbose_name="Name of the workgroup",
    )

    description = models.TextField(
        verbose_name="Desc.",
        null=True,
    )

    members = models.ManyToManyField(
        "peoples.People",
        verbose_name="Team members",
        related_name="projects",
        related_query_name="projects",
    )

    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Projects"
        ordering = ("label",)


class People(Administrable):
    last_name = models.CharField(
        max_length=255,
        verbose_name="last name",
    )

    first_name = models.CharField(
        max_length=255,
        verbose_name="first name",
    )

    civility = models.ForeignKey(
        "peoples.civility",
        on_delete=models.PROTECT,
        verbose_name="FK to civility table",
    )

    job = models.ForeignKey(
        "peoples.job",
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="A people has 1 job",
    )

    email = models.EmailField(
        verbose_name="email address",
    )

    phone_number = models.CharField(
        max_length=50,
        null=True,
        verbose_name="Phone number",
    )

    class Meta:
        verbose_name = "People"
        verbose_name_plural = "Peoples"
        ordering = (
            "last_name",
            "first_name",
        )
