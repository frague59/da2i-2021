"""
Urls for the :mod:`peoples` application
"""
from peoples import views
from django.urls import path

urlpatterns = [
    path("people/", views.PeopleListView.as_view(), name="people_list"),
]
