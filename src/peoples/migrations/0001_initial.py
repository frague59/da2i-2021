# Generated by Django 3.1.6 on 2021-02-13 17:10

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Civility',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='updated at')),
                ('label', models.CharField(max_length=255, verbose_name='label')),
                ('label_short', models.CharField(blank=True, max_length=255, null=True, verbose_name='Short version, aka. M. for Mister')),
                ('created_by', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.PROTECT, related_name='peoples_civility_creators', to=settings.AUTH_USER_MODEL, verbose_name='creator')),
                ('updated_by', models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='peoples_civility_updaters', to=settings.AUTH_USER_MODEL, verbose_name='updater')),
            ],
            options={
                'verbose_name': 'Civility',
                'verbose_name_plural': 'Civilities',
                'ordering': ('label',),
            },
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='updated at')),
                ('label', models.CharField(max_length=255)),
                ('description', models.TextField(null=True, verbose_name='Desc.')),
                ('created_by', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.PROTECT, related_name='peoples_job_creators', to=settings.AUTH_USER_MODEL, verbose_name='creator')),
                ('updated_by', models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='peoples_job_updaters', to=settings.AUTH_USER_MODEL, verbose_name='updater')),
            ],
            options={
                'verbose_name': 'Job',
                'verbose_name_plural': 'Jobs',
                'ordering': ('label',),
            },
        ),
        migrations.CreateModel(
            name='People',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='updated at')),
                ('last_name', models.CharField(max_length=255, verbose_name='last name')),
                ('first_name', models.CharField(max_length=255, verbose_name='first name')),
                ('email', models.EmailField(max_length=254, verbose_name='email address')),
                ('phone_number', models.CharField(max_length=50, null=True, verbose_name='Phone number')),
                ('civility', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='peoples.civility', verbose_name='FK to civility table')),
                ('created_by', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.PROTECT, related_name='peoples_people_creators', to=settings.AUTH_USER_MODEL, verbose_name='creator')),
                ('job', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='peoples.job', verbose_name='A people has 1 job')),
                ('updated_by', models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='peoples_people_updaters', to=settings.AUTH_USER_MODEL, verbose_name='updater')),
            ],
            options={
                'verbose_name': 'People',
                'verbose_name_plural': 'Peoples',
                'ordering': ('last_name', 'first_name'),
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='updated at')),
                ('label', models.CharField(max_length=255, verbose_name='Name of the workgroup')),
                ('description', models.TextField(null=True, verbose_name='Desc.')),
                ('created_by', models.ForeignKey(editable=False, on_delete=django.db.models.deletion.PROTECT, related_name='peoples_project_creators', to=settings.AUTH_USER_MODEL, verbose_name='creator')),
                ('members', models.ManyToManyField(related_name='projects', related_query_name='projects', to='peoples.People', verbose_name='Team members')),
                ('updated_by', models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='peoples_project_updaters', to=settings.AUTH_USER_MODEL, verbose_name='updater')),
            ],
            options={
                'verbose_name': 'Project',
                'verbose_name_plural': 'Projects',
                'ordering': ('label',),
            },
        ),
    ]
