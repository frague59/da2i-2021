"""
Settings for the dev platform

This settings will override the PROD settings
"""
from .base import *  # noqa

DEBUG = True
DEBUG_TOOLBAR = True


if DEBUG_TOOLBAR:
    INSTALLED_APPS.append("debug_toolbar")  # noqa
    MIDDLEWARE.insert(  # noqa
        0,
        "debug_toolbar.middleware.DebugToolbarMiddleware",
    )
    INTERNAL_IPS = [
        "127.0.0.1",
        "localhost",
    ]

# It's possible to override the default database for DEV !
# So you are sure not to scratch your pretty production database...
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "directory-dev.sqlite3",  # noqa
    }
}
